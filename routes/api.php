<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'Auth\AuthController@login');
    Route::post('signup', 'Auth\AuthController@signup');
    Route::get('error', 'Auth\AuthController@error');

    Route::group(['middleware' => 'auth:api'], function() {
        //Route::get('logout', 'Auth\AuthController@logout');
        Route::get('get_user', 'Auth\AuthController@getUser');
        Route::get('get_organizations', 'Organization\OrgController@getOrganizations');
        Route::post('save_organization', 'Organization\OrgController@saveOrganization');
    });
});
