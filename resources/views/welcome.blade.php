<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
    <h2>Api Regitrar nuevo usuario</h2>
    <pre>
        POST /api/auth/signup HTTP/1.1
        Host: smart.ivanmv.com
        Content-Type: application/json
        User-Agent: PostmanRuntime/7.17.1
        Accept: */*
        Cache-Control: no-cache
        Postman-Token: bf7ccfe2-e62c-436d-85ba-f51fca47fdec,442ac6d0-f586-4352-8fad-7b8405ec1001
        Host: smart.ivanmv.com
        Accept-Encoding: gzip, deflate
        Content-Length: 132
        Connection: keep-alive
        cache-control: no-cache

        {
            "password": "hola",
            "password_confirmation": "hola1",
            "name": "Iván Morán",
            "email": "ivanmv.1987+2@gmail.com"
        }
    </pre>
    <h2>Api Login</h2>
    <pre>
        POST /api/auth/login HTTP/1.1
        Host: smart.ivanmv.com
        Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW
        User-Agent: PostmanRuntime/7.17.1
        Accept: */*
        Cache-Control: no-cache
        Postman-Token: 31acb865-4deb-4f83-a380-e456788f3c31,ea8fe8b8-65ec-4833-a28c-0763636acb31
        Host: smart.ivanmv.com
        Accept-Encoding: gzip, deflate
        Content-Length: 292
        Connection: keep-alive
        cache-control: no-cache


        Content-Disposition: form-data; name="email"

        ivanmv.1987@gmail.com
        ------WebKitFormBoundary7MA4YWxkTrZu0gW--,
        Content-Disposition: form-data; name="email"

        ivanmv.1987@gmail.com
        ------WebKitFormBoundary7MA4YWxkTrZu0gW--
        Content-Disposition: form-data; name="password"

        hola
        ------WebKitFormBoundary7MA4YWxkTrZu0gW--
    </pre>
    <h2>Obtener información del usuario</h2>
    <pre>
        GET /api/auth/get_user HTTP/1.1
        Host: smart.ivanmv.com
        Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjlkYTY1ZmRmMzI0YjA5MzYzYjI3YWViNGFmY2U1MTA3M2QzMzJjZmE1YjgwYjg4NzIxOTUxMzc5MjYyNmRkNGY2NjljZGU0YTUzNDcyOWU5In0.eyJhdWQiOiIzIiwianRpIjoiOWRhNjVmZGYzMjRiMDkzNjNiMjdhZWI0YWZjZTUxMDczZDMzMmNmYTViODBiODg3MjE5NTEzNzkyNjI2ZGQ0ZjY2OWNkZTRhNTM0NzI5ZTkiLCJpYXQiOjE1Njk2MzczMjAsIm5iZiI6MTU2OTYzNzMyMCwiZXhwIjoxNjAxMjU5NzIwLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.YKbNhHOB23sAgn6WIdhXHQ7lubjylCMq6oCTvdvXfVHfIKsJpkhL8igpNAwchqtjq_33sYQVYp8CANVRZf19l9YRSWHHWxGKWbcisFHZFx3gePHhVTPSpbGojBu5ByXnSltt7ghXizsHdTfq9XFDPJBQV3ijsiB6GwrU8yH6nvlIVWyY83J3tfjQaYNw0kEc18KPw-08aTN12OfH0otqRAFew6fkbL3GtVKR1No18LZsd6257DaGZqo9gt9R78arLnM2TxrmkiH7eDZTE9vkXfLJGhalaa6OFai4_ztugdDH3FFSbuXw3qoZNOf-KMmvZTxbHlf41C8pENgZ6joVfR3JN2GoYOAhAKymCj5fPYmT3NOk3LgxBUESlo_iX3Va91RV7-6XTq2SiKcjEFiiWrJDryc7YEYPNe0lxJL9ZLpfhgmO2Sv1gEFvpeCllJYUIqggQo8XhkO-gHPQ_wSdRgsEaBS76r0FEkNBJnCiy52pCfOjPw7P1Pm5KUVaCuG7t8vYuZDBtPyhJ9aauE4Hy0lm848M9Gvhuyx0dsPyQifvCgO50Ctlc7RmoiC2tVNSJqbCi7yTy8c1tfGx7eLTKeN6GhJIRqzGY9SU1NYMfZV-44g3NnKmobtDc_M_EWkMzfr0uYOs1k4YMw7oZ2jpZFk-Q9IEkGA-1rtV3C7LkrM
        User-Agent: PostmanRuntime/7.17.1
        Accept: */*
        Cache-Control: no-cache
        Postman-Token: a3876168-a423-4086-adfa-b42f0ebd90e3,d3ad07f3-13a4-4c7d-b6ac-d29da3718484
        Host: localhost
        Accept-Encoding: gzip, deflate
        Cookie: PHPSESSID=takj7fvpk6u0nead2b8a44bqff
        Connection: keep-alive
        cache-control: no-cache
    </pre>
    <h2>Obtener listado de organizaciones</h2>
    <pre>
        GET /api/auth/get_organizations HTTP/1.1
        Host: smart.ivanmv.com
        Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImI0N2MwMzhhNTI5ZjgwMTZjMTM4YTgwNmRjYzVkY2I5NjgyMDQ2NGNhNDdjM2MyYzdkZjljY2EzNjk4MDBjYWI1ZDkyOThjYzU4YjM5MGMxIn0.eyJhdWQiOiIxIiwianRpIjoiYjQ3YzAzOGE1MjlmODAxNmMxMzhhODA2ZGNjNWRjYjk2ODIwNDY0Y2E0N2MzYzJjN2RmOWNjYTM2OTgwMGNhYjVkOTI5OGNjNThiMzkwYzEiLCJpYXQiOjE1Njk3MDA4NzcsIm5iZiI6MTU2OTcwMDg3NywiZXhwIjoxNjAxMzIzMjc3LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.bChSOPiAdQxHt2cDUVJeCfPHoQrEnvrkcoVsbzbrXtYmdBqaXwt9s1LyN_R17ASu9EIAZXaBhLEC13oi7SxE-EY13aERlfP5vhBWc9vhdTmLBag-5fjBiXoSVh9ij3E7PQOisdrhFlbyNvxQHLZ0zGLyiqQ8guJGmOUwGQwxVCAalHDKz2KOG8OxahEHXNShxQebTI0oa1aEAO1ZR0jFHux6hRlGJw3czPlVT0SqtHNSm0asNxex3zq_Z3RWIuclxVIZwo-6LEOwXQa3cdG1SL_poxGeMOSfkXEEe7X0mpPRAlhwHIOC0rISYY3Xn5bZZ5ipXc-bcRaNMQBjWLOMpbcFk2CBdk4VHo5F8YXhwhYEDk1iChq9zmDgCk97nGEoUHsNMtgYEr1m3vA4WuMt3can9A7lLBz3VdVAuhB4LJo2KQpz8L-GTwWnpEMAPhzhw_pT9e41Mo0dNRQqBBxb0iGam4p5lvNiG9FYcqPPMWbuEuxrj_K-kpApIET5E1P_1qvylT3hbuMpqkhLxBV3ITkP0le7dTjE6RHzXGQyvPQiIOelsawLJaEqnZpzg4dQsJUFryxzHjFj70eO18C-B8RXYiIQYm-Ki95I7_RzjEstTT-Y22AH_VQzTOsNDm0rJdDiErH12lf8qx_QuYiNhsJd_sy7hcLESNn0v3NIKNE
        User-Agent: PostmanRuntime/7.17.1
        Accept: */*
        Cache-Control: no-cache
        Postman-Token: 1eae8781-4e7e-4cc3-b5f9-fcd26cc25e2a,6b0fc444-672d-439b-840a-767eebb2e567
        Host: smart.ivanmv.com
        Accept-Encoding: gzip, deflate
        Connection: keep-alive
        cache-control: no-cache
    </pre>
    <h2>Registrar organización</h2>
    <pre>
        POST /api/auth/save_organization HTTP/1.1
        Host: smart.ivanmv.com
        Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjlkYTY1ZmRmMzI0YjA5MzYzYjI3YWViNGFmY2U1MTA3M2QzMzJjZmE1YjgwYjg4NzIxOTUxMzc5MjYyNmRkNGY2NjljZGU0YTUzNDcyOWU5In0.eyJhdWQiOiIzIiwianRpIjoiOWRhNjVmZGYzMjRiMDkzNjNiMjdhZWI0YWZjZTUxMDczZDMzMmNmYTViODBiODg3MjE5NTEzNzkyNjI2ZGQ0ZjY2OWNkZTRhNTM0NzI5ZTkiLCJpYXQiOjE1Njk2MzczMjAsIm5iZiI6MTU2OTYzNzMyMCwiZXhwIjoxNjAxMjU5NzIwLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.YKbNhHOB23sAgn6WIdhXHQ7lubjylCMq6oCTvdvXfVHfIKsJpkhL8igpNAwchqtjq_33sYQVYp8CANVRZf19l9YRSWHHWxGKWbcisFHZFx3gePHhVTPSpbGojBu5ByXnSltt7ghXizsHdTfq9XFDPJBQV3ijsiB6GwrU8yH6nvlIVWyY83J3tfjQaYNw0kEc18KPw-08aTN12OfH0otqRAFew6fkbL3GtVKR1No18LZsd6257DaGZqo9gt9R78arLnM2TxrmkiH7eDZTE9vkXfLJGhalaa6OFai4_ztugdDH3FFSbuXw3qoZNOf-KMmvZTxbHlf41C8pENgZ6joVfR3JN2GoYOAhAKymCj5fPYmT3NOk3LgxBUESlo_iX3Va91RV7-6XTq2SiKcjEFiiWrJDryc7YEYPNe0lxJL9ZLpfhgmO2Sv1gEFvpeCllJYUIqggQo8XhkO-gHPQ_wSdRgsEaBS76r0FEkNBJnCiy52pCfOjPw7P1Pm5KUVaCuG7t8vYuZDBtPyhJ9aauE4Hy0lm848M9Gvhuyx0dsPyQifvCgO50Ctlc7RmoiC2tVNSJqbCi7yTy8c1tfGx7eLTKeN6GhJIRqzGY9SU1NYMfZV-44g3NnKmobtDc_M_EWkMzfr0uYOs1k4YMw7oZ2jpZFk-Q9IEkGA-1rtV3C7LkrM
        User-Agent: PostmanRuntime/7.17.1
        Accept: */*
        Cache-Control: no-cache
        Postman-Token: d4003df3-bac2-4196-a0dc-e853badef10e,b24d1b61-b3fa-4170-8a90-c31c835a0ad2
        Host: smart.ivanmv.com
        Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW
        Accept-Encoding: gzip, deflate
        Content-Length: 486
        Connection: keep-alive
        cache-control: no-cache


        Content-Disposition: form-data; name="name"

        1
        ------WebKitFormBoundary7MA4YWxkTrZu0gW--,
        Content-Disposition: form-data; name="name"

        1
        ------WebKitFormBoundary7MA4YWxkTrZu0gW--
        Content-Disposition: form-data; name="address"

        2
        ------WebKitFormBoundary7MA4YWxkTrZu0gW--,
        Content-Disposition: form-data; name="name"

        1
        ------WebKitFormBoundary7MA4YWxkTrZu0gW--
        Content-Disposition: form-data; name="address"

        2
        ------WebKitFormBoundary7MA4YWxkTrZu0gW--
        Content-Disposition: form-data; name="phone"

        3
        ------WebKitFormBoundary7MA4YWxkTrZu0gW--,
        Content-Disposition: form-data; name="name"

        1
        ------WebKitFormBoundary7MA4YWxkTrZu0gW--
        Content-Disposition: form-data; name="address"

        2
        ------WebKitFormBoundary7MA4YWxkTrZu0gW--
        Content-Disposition: form-data; name="phone"

        3
        ------WebKitFormBoundary7MA4YWxkTrZu0gW--
        Content-Disposition: form-data; name="encriptionCode"

        4
        ------WebKitFormBoundary7MA4YWxkTrZu0gW--
    </pre>
    </body>
</html>
