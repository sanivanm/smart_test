<?php


namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Storage;
use Avatar;
use App\Notifications\SignupActivate;
use App\Notifications\SignupActivated;
use App\User;

class AuthController extends Controller {

    /**
     * Create user deactivate and send notification to activate account user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {
        $data["status"] = "error";
        $validator = Validator::make($request->all(),
            [
                'name' => 'required|string',
                'email' => 'required|string|email|unique:users',
                'password' => 'required|string|confirmed'
            ]);
        if ($validator->fails()) {
            $data["message"] = $validator->errors();
            return response()->json($data, 400);
        }

        $data["status"] = "success";
        $data["message"] = "Registrado correctamente";
        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        $user->save();
        return response()->json($data, 200);
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $status = "error";
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return response()->json([
                'status'=>$status,
                'message' => 'Usuario o contraseña incorrecto'
            ], 400);
        $user = $request->user();
        $tokenResult = $user->createToken(env("PERSONAL_ACCESS"));
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(52);
        $token->save();
        $status = "success";
        return response()->json([
            'status'=>$status,
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'user'=>$request->user(),
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    public function getUser(Request $request){
        if(Auth::guest()){
            return response()->json(["message"=>"Sin autorización"]);
        }else{
            return response()->json($request->user());
        }
    }

    public function error(){
        return response()->json(["status"=>"error","message"=>"Sin autorización"]);
    }

}