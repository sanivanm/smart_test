<?php


namespace App\Http\Controllers\Organization;
use \App\Http\Controllers\Controller;
use App\Http\Models\Organization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class OrgController  extends Controller{

    public function getOrganizations() {
        $data["status"] = "success";
        $data["organizations"] = Organization::getOrganizations();
        return response()->json($data);
    }

    public function saveOrganization(Request $request){
        $data["status"] = "error";

        $validator = Validator::make($request->all(),
            [
                'name' => 'required|string',
                'address' => 'required|string',
                'phone' => 'required|string'
            ]);
        if ($validator->fails()) {
            $data["message"] = $validator->errors();
            return response()->json($data, 400);
        }

        $first4Letters = substr($request->name, 0, 4);
        $first4Letters = str_pad($first4Letters, 4, "X", STR_PAD_LEFT);;
        $last4Numbers = substr($request->phone, strlen($request->phone) - 4, 4);
        $last4Numbers = str_pad($last4Numbers, 4, "0", STR_PAD_LEFT);;
        $lastId = Organization::max('id') + 1;
        $lastId = str_pad($lastId, 3, "0", STR_PAD_LEFT);
        $externalId = strtoupper($first4Letters).strtoupper($last4Numbers)."NE".strtoupper($lastId);
        $saved = Organization::insert([
            "userId"=>\Auth()->user()->id,
            "name"=>$request->name,
            "address"=>$request->address,
            "phone"=>$request->phone,
            "encriptionCode"=>$request->encriptionCode,
            "external_id"=>$externalId
        ]);
        if ($saved){
            $data["status"] = "success";
        }

        return response()->json($data);
    }
}