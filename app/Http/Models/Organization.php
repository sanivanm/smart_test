<?php


namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model {
    protected $table = 'organizations';
    protected $primaryKey = 'id';
    protected $fillable = ['userId', 'name', 'address', 'phone', 'external_id', 'encriptionCode'];

    public static function getOrganizations()
    {
        return Organization::get();
    }

}